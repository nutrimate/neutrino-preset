const SentryWebpackPlugin = require('@sentry/webpack-plugin')

const sentry = () => neutrino => {
  if (!process.env.SENTRY_ENV || process.env.SENTRY_ENV === 'local') {
    return
  }

  neutrino.config
    .plugin('sentry')
    .use(
      SentryWebpackPlugin,
      [
        {
          include: ['build', 'src']
        }
      ]
    )
}

module.exports = sentry
