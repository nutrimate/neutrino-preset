const standard = require('@neutrinojs/standardjs')
const react = require('@neutrinojs/react')
const jest = require('@neutrinojs/jest')
const sentry = require('./sentry')

const createModuleResolverConfig = (rootAlias) => ({
  alias: {
    [`^${rootAlias}/(.+)`]: './src/\\1'
  },
  extensions: ['', '.js', '.jsx', '.ts', '.tsx']
})

module.exports = (options = {}) => neutrino => {
  const {
    typescript: typescriptOptions = { project: undefined, rootPathAlias: '@_' },
    react: reactOptions = {
      title: 'React app',
      env: {}
    },
    jest: jestOptions = {
      setupFilesAfterEnv: [
        '<rootDir>/src/tests/setup.ts'
      ]
    }
  } = options

  const moduleResolverConfig = createModuleResolverConfig(typescriptOptions.rootPathAlias)

  const presets = [
    standard({
      eslint: {
        baseConfig: {
          extends: [
            '@nutrimate'
          ],
          parser: '@typescript-eslint/parser',
          parserOptions: {
            project: typescriptOptions.project
          },
          settings: {
            'import/resolver': {
              'babel-module': moduleResolverConfig
            }
          }
        }
      }
    }),
    react({
      html: {
        title: reactOptions.title
      },
      env: reactOptions.env,
      babel: {
        presets: [
          '@babel/typescript'
        ],
        plugins: [
          [
            'babel-plugin-module-resolver',
            moduleResolverConfig
          ]
        ]
      },
      devServer: {
        port: process.env.PORT || 3000
      }
    }),
    jest(jestOptions),
    sentry()
  ]

  presets.forEach(preset => neutrino.use(preset))
}
